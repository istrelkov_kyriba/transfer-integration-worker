/********************************************************************************
 * Copyright 2020 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.transfer.file.worker.entity;

import org.springframework.batch.core.BatchStatus;

import java.util.Date;


/**
 * @author M-IST
 */
public class Result
{
  private Date startDateTime;
  private Date finishStartDateTime;
  private String fileName;
  private BatchStatus status;

  public Result()
  {

  }


  public Result(Date startDateTime, Date finishStartDateTime, String fileName, BatchStatus status)
  {
    this.startDateTime = startDateTime;
    this.finishStartDateTime = finishStartDateTime;
    this.fileName = fileName;
    this.status = status;
  }


  public Date getStartDateTime()
  {
    return startDateTime;
  }


  public void setStartDateTime(Date startDateTime)
  {
    this.startDateTime = startDateTime;
  }


  public Date getFinishStartDateTime()
  {
    return finishStartDateTime;
  }


  public void setFinishStartDateTime(Date finishStartDateTime)
  {
    this.finishStartDateTime = finishStartDateTime;
  }


  public String getFileName()
  {
    return fileName;
  }


  public void setFileName(String fileName)
  {
    this.fileName = fileName;
  }


  public BatchStatus getStatus()
  {
    return status;
  }


  public void setStatus(BatchStatus status)
  {
    this.status = status;
  }
}