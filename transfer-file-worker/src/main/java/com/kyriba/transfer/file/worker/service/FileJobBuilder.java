/********************************************************************************
 * Copyright 2020 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.transfer.file.worker.service;

import com.kyriba.transfer.file.worker.dto.TransferDTO;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


/**
 * @author M-IST
 */
@Service
public class FileJobBuilder
{
  @Autowired
  public JobBuilderFactory jobBuilderFactory;

  @Autowired
  public StepBuilderFactory stepBuilderFactory;

  @Autowired
  private KafkaTemplate<Long, TransferDTO> kafkaTemplate;

  @Autowired
  private ItemWriter transferWriter;


  public Job createJob(String fileName)
  {
    return jobBuilderFactory.get("integrateFileJob")
        .incrementer(new RunIdIncrementer())
        .flow(createStep(fileName))
        .end()
        .build();
  }


  private Step createStep(String fileName)
  {
    return stepBuilderFactory.get("step1")
        .<TransferDTO, TransferDTO>chunk(1000)
        .reader(createReader(fileName))
        .writer(transferWriter)
        .build();
  }


  private FlatFileItemReader<TransferDTO> createReader(String fileName)
  {
    return new FlatFileItemReaderBuilder<TransferDTO>()
        .name("transfersReader")
        .resource(new ClassPathResource(fileName))
        .delimited()
        .delimiter("|")
        .names(new String[] {
            "accountNumber", "paymentType", "budgetCode", "valueDate", "currency", "amount", "vendorName", "vendorAddress",
            "vendorCity", "vendorState", "vendorCountry", "vendorZipCode", "vendorTaxID", "vendorEmail", "bankName",
            "bankAddress", "bankCity", "bankState", "bankCountry", "bankZipCode", "bankAccountNumber", "bankRoutingCode",
            "bankSwiftBic", "intermediaryDescription", "intermediaryBic", "intermediaryCountry", "intermediaryAccount",
            "correspondentDescription", "correspondentBic", "correspondentCountry", "correspondentAccount", "reference",
            "remittanceIdentifer1", "remmitanceIdentifer2", "memo", "reason1", "reason2", "reason3", "reason4", "freeText1",
            "freeText2", "freeText3", "uzerZone1", "uzerZone2", "uzerZone3", "uzerZone4", "uzerZone5"
        })
        .fieldSetMapper(new BeanWrapperFieldSetMapper<TransferDTO>()
        {{
          setTargetType(TransferDTO.class);
        }})
        .build();
  }
}