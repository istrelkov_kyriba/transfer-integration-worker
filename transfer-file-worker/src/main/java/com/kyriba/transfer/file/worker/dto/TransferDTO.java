/********************************************************************************
 * Copyright 2020 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.transfer.file.worker.dto;

import lombok.Data;


/**
 * @author M-IST
 */
@Data
public class TransferDTO
{
  /*
    Account Number|Payment Type|Budget Code|Value Date|Currency|Amount|Vendor Name|Vendor Address|Vendor City|Vendor State|
    Vendor Country|Vendor Zip Code|Vendor Tax ID|Vendor Email|Bank Name|Bank Address|Bank City|Bank State|Bank Country|
    Bank ZIP Code|Bank Account number|Bank Routing Code|Bank SWIFT BIC|Intermediary Description|Intermediary BIC|
    Intermediary Country|Intermediary Account|Correspondent Description|Correspondent BIC|Correspondent Country|
    Correspondent Account|Reference|Transfer/Remittance Identifer 1|Transfer/Remittance Identifer 2|Memo|Reason 1|
    Reason 2|Reason 3|Reason 4|Free Text 1|Free Text 2|Free Text 3|User Zone 1|User Zone 2|User Zone 3|User Zone 4|
    User Zone 5
  */
  private String accountNumber;
  private String paymentType;
  private String budgetCode;
  private String valueDate;
  private String currency;
  private String amount;
  private String vendorName;
  private String vendorAddress;
  private String vendorCity;
  private String vendorState;
  private String vendorCountry;
  private String vendorZipCode;
  private String vendorTaxID;
  private String vendorEmail;
  private String bankName;
  private String bankAddress;
  private String bankCity;
  private String bankState;
  private String bankCountry;
  private String bankZipCode;
  private String bankAccountNumber;
  private String bankRoutingCode;
  private String bankSwiftBic;
  private String intermediaryDescription;
  private String intermediaryBic;
  private String intermediaryCountry;
  private String intermediaryAccount;
  private String correspondentDescription;
  private String correspondentBic;
  private String correspondentCountry;
  private String correspondentAccount;
  private String reference;
  private String remittanceIdentifer1;
  private String remmitanceIdentifer2;
  private String memo;
  private String reason1;
  private String reason2;
  private String reason3;
  private String reason4;
  private String freeText1;
  private String freeText2;
  private String freeText3;
  private String uzerZone1;
  private String uzerZone2;
  private String uzerZone3;
  private String uzerZone4;
  private String uzerZone5;
}