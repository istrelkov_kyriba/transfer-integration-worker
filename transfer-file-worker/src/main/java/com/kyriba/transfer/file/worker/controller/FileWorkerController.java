/********************************************************************************
 * Copyright 2020 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.transfer.file.worker.controller;

import com.kyriba.transfer.file.worker.entity.FileConfig;
import com.kyriba.transfer.file.worker.entity.Result;
import com.kyriba.transfer.file.worker.service.FileJobBuilder;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;


/**
 * @author M-IST
 */
@RestController
public class FileWorkerController
{
  @Autowired
  private JobLauncher jobLauncher;

  @Autowired
  private FileJobBuilder fileJobBuilder;


  @PostMapping("run")
  public Result runFileProcessing(@RequestBody FileConfig file) throws JobParametersInvalidException,
      JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException,
      FileNotFoundException
  {
    ClassPathResource fileResource = new ClassPathResource(file.getFileName());
    if (!fileResource.exists())
      throw new FileNotFoundException(file.getFileName() + " not found");

    JobParameters params = new JobParametersBuilder()
        .addString("JobID", String.valueOf(System.currentTimeMillis()))
        .toJobParameters();
    JobExecution jobExecution = jobLauncher.run(fileJobBuilder.createJob(file.getFileName()), params);

    return new Result(jobExecution.getStartTime(), jobExecution.getEndTime(), file.getFileName(), jobExecution.getStatus());
  }


  @ExceptionHandler(Exception.class)
  public String handleError(Exception e)
  {
    return e.getMessage();
  }
}