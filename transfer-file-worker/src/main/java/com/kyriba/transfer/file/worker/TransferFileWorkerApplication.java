package com.kyriba.transfer.file.worker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class})
public class TransferFileWorkerApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(TransferFileWorkerApplication.class, args);
	}
}
