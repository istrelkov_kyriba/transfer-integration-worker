DOCKER_REPOSITORY?=transfer-integration-worker
DOCKER_TAG?=0.0.1-dev

K8S_NAMESPACE?=poc-tiw
K8S_RELEASE?=v0.0.1
K8S_CONTEXT?=

build: docker

docker:
	@cd transfer-integration-worker && docker build -t $(DOCKER_REPOSITORY):$(DOCKER_TAG) .

deploy:
	@kubectl create namespace $(K8S_NAMESPACE) --context=$(K8S_CONTEXT) && echo "Namespace created" || echo "Namespace not created"

	helm upgrade --install $(K8S_RELEASE) ./transfer-integration-chart --namespace=$(K8S_NAMESPACE) --kube-context=$(K8S_CONTEXT) \
		--debug \
		--set fileWorker.image.tag=$(DOCKER_TAG) \
		--set transferIntegrator.image.tag=$(DOCKER_TAG) \


