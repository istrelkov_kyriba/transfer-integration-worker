/********************************************************************************
 * Copyright 2020 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.transfer.integration.worker.config;

import com.kyriba.transfer.integration.worker.object.Transfer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;


/**
 * @author M-IST
 */
@Configuration
public class KafkaConsumerConfig
{
  @Value(value = "${kafka.bootstrap.address}")
  private String bootstrapAddress;

  @Value(value = "${kafka.topic.name}")
  private String topicName;


  @Bean
  public ConsumerFactory<Long, Transfer> transferConsumerFactory()
  {
    Map<String, Object> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
    props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
    return new DefaultKafkaConsumerFactory<>(props, new LongDeserializer(),
        new JsonDeserializer<>(Transfer.class, false));
  }


  @Bean
  public ConcurrentKafkaListenerContainerFactory<Long, Transfer> transferKafkaListenerContainerFactory()
  {
    ConcurrentKafkaListenerContainerFactory<Long, Transfer> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(transferConsumerFactory());
    return factory;
  }
}