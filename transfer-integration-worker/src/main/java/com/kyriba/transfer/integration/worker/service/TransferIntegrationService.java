/********************************************************************************
 * Copyright 2020 Kyriba Corp. All Rights Reserved.                             *
 * The content of this file is copyrighted by Kyriba Corporation and can not be *
 * reproduced, distributed, altered or used in any form, in whole or in part.   *
 *******************************************************************************/
package com.kyriba.transfer.integration.worker.service;

import com.kyriba.transfer.integration.worker.object.Transfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;


/**
 * @author M-IST
 */
@Service
public class TransferIntegrationService
{
  @Autowired
  private KafkaTemplate<Long, Transfer> kafkaTemplate;

  @Value(value = "${min.delay.ms}")
  private long minDelayMS;

  @Value(value = "${max.delay.ms}")
  private long maxDelayMS;
  
  @KafkaListener(id = "${kafka.listener.id}", topics = "${kafka.topic.name}",containerFactory = "transferKafkaListenerContainerFactory")
  public void consume(Transfer transfer) throws InterruptedException
  {
    if (minDelayMS > 0) {
      TimeUnit.MILLISECONDS.sleep(minDelayMS + ((long) (Math.random() * (maxDelayMS - minDelayMS))));
    }

    kafkaTemplate.sendDefault(System.currentTimeMillis(), transfer);
  }
}