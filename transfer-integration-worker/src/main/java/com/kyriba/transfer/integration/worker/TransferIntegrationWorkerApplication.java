package com.kyriba.transfer.integration.worker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransferIntegrationWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransferIntegrationWorkerApplication.class, args);
	}
}
