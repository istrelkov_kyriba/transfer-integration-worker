# README #

## Transfer file worker
- Transfer file worker project processes file and sent rows to kafka
- To order to start reading the file send post request to /run path with {"fileName":"Transfers.txt"}  

### Transfer file worker is gradle project 
- The list of supported commands can be found here https://docs.gradle.org/current/userguide/command_line_interface.html
- E.g. to order to build project you need run: gradle build

### To order to build and load docker image to repository run the following commands:

- docker build -t transfer-file-worker-0.0.1 .
- docker tag transfer-file-worker-0.0.1 kyribastrelkov/transfer-file-worker
- docker push kyribastrelkov/transfer-file-worker

## Transfer integration worker
Transfer integration worker project listens kafka, processes rows with delay and sends back to kafka

### Transfer integration worker is gradle project 
- The list of supported commands can be found here https://docs.gradle.org/current/userguide/command_line_interface.html
- E.g. to order to build project you need run: gradle build


### To order to build and load docker image to repository run the following commands:
- docker build -t transfer-integration-worker-0.0.1 .
- docker tag transfer-integration-worker-0.0.1 kyribastrelkov/transfer-integration-worker
- docker push kyribastrelkov/transfer-integration-worker



# Using Makefile

Building using Makefile and `make`

```shell script
DOCKER_REPOSTORY=<some repo> DOCKER_TAG=dev make docker 
```

